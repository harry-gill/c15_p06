package au.com.harrygill;

import java.util.List;
import java.util.Optional;

public class Example {

    public static void main(String[] args) {

        Optional<String> name = Optional.ofNullable("Java");

        //System.out.println(name.get());

        Optional<String> empty = Optional.empty();

        /*if (empty.isPresent()) {
            System.out.println(empty.get());
        }*/

        //empty.ifPresent(System.out::println);
        //name.ifPresent(System.out::println);

        //System.out.println(name.orElse("Default"));
        //System.out.println(empty.orElse("Default"));

        //System.out.println(name.orElseGet(() -> "Default"));
        //System.out.println(empty.orElseGet(() -> "Default"));

        //System.out.println(name.orElseThrow());
        //System.out.println(empty.orElseThrow());

        //System.out.println(name.orElseThrow(() -> new RuntimeException()));
        //System.out.println(empty.orElseThrow(RuntimeException::new));

        Optional<List<String>> retrievedProfiles = Optional.ofNullable(null);
        Optional<List<String>> retrievedEmailProfiles = Optional.ofNullable(null);

        retrievedEmailProfiles.ifPresent(retrievedEmailProfiles1 -> retrievedProfiles.ifPresent(r -> r.addAll(retrievedEmailProfiles1)));

    }
}